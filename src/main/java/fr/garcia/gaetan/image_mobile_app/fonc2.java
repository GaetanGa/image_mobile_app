package fr.garcia.gaetan.image_mobile_app;

import androidx.appcompat.app.AppCompatActivity;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class fonc2 extends AppCompatActivity {

    private static final MediaType MEDIA_TYPE_PLAINTEXT = MediaType.parse("application/octet-stream");
    private final OkHttpClient client = new OkHttpClient();
    private Button mButtonYesImg1;
    private Button mButtonYesImg2;
    private Button mButtonYesImg3;
    private Button mButtonNoImg1;
    private Button mButtonNoImg2;
    private Button mButtonNoImg3;
    private Button mButtonDone;
    private Boolean feedbackimg1;
    private Boolean feedbackimg2;
    private Boolean feedbackimg3;
    private int code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fonc2);

        mButtonYesImg1 = (Button) findViewById(R.id.activity_fonc2_button_yes_img1);
        mButtonYesImg2 = (Button) findViewById(R.id.activity_fonc2_button_yes_img2);
        mButtonYesImg3 = (Button) findViewById(R.id.activity_fonc2_button_yes_img3);
        mButtonNoImg1 = (Button) findViewById(R.id.activity_fonc2_button_no_img1);
        mButtonNoImg2 = (Button) findViewById(R.id.activity_fonc2_button_no_img2);
        mButtonNoImg3 = (Button) findViewById(R.id.activity_fonc2_button_no_img3);
        mButtonDone = (Button) findViewById(R.id.activity_fonc2_button_done);

        Bundle extra = getIntent().getExtras();
        code = Integer.parseInt(extra.getString("code"));


        mButtonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (feedbackimg1 == Boolean.TRUE) {
                    sendFeedback sendFeedback = new sendFeedback(1, 1, code);
                    sendFeedback.execute();

                } else if (feedbackimg1 == Boolean.FALSE){
                    sendFeedback sendFeedback = new sendFeedback(0, 1, code);
                    sendFeedback.execute();
                }


                if (feedbackimg2 == Boolean.TRUE){
                    sendFeedback sendFeedback = new sendFeedback(1, 2, code);
                    sendFeedback.execute();

                } else if (feedbackimg2 == Boolean.FALSE){
                    sendFeedback sendFeedback = new sendFeedback(0, 2, code);
                    sendFeedback.execute();
                }


                if (feedbackimg3 == Boolean.TRUE){
                    sendFeedback sendFeedback = new sendFeedback(1, 3, code);
                    sendFeedback.execute();

                } else if (feedbackimg3 == Boolean.FALSE){
                    sendFeedback sendFeedback = new sendFeedback(0, 3, code);
                    sendFeedback.execute();
                }


                Intent output = new Intent();
                setResult(4, output);
                finish();

            }
        });


        mButtonYesImg1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                feedbackimg1 = true;
            }
        });

        mButtonYesImg2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                feedbackimg2 = true;
            }
        });

        mButtonYesImg3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                feedbackimg3 = true;
            }
        });

        mButtonNoImg1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                feedbackimg1 = false;
            }
        });

        mButtonNoImg2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                feedbackimg2 = false;
            }
        });

        mButtonNoImg3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                feedbackimg3 = false;
            }
        });

    }

    //POST the feedback for each image from the result
    private class sendFeedback extends AsyncTask<String, Void, Void>{
        int result;
        int i;
        int code;

        public sendFeedback(int bool, int num, int requestCode){
            this.result = bool;
            this.i = num;
            this.code = requestCode;
        }


        @Override
        protected Void doInBackground(String... strings) {

            JSONObject postData = new JSONObject();

            try{
                postData.put("result", result);
            } catch (JSONException e){
                e.printStackTrace();
            }

            Request request = new Request.Builder()
                    .url("http://192.168.0.45:8000/apiImage/feedback"+this.i+"/"+this.code+"/")
                    .post(RequestBody.create(postData.toString(), MEDIA_TYPE_PLAINTEXT))
                    .build();

            try {
                client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
