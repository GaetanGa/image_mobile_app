package fr.garcia.gaetan.image_mobile_app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class fonc1 extends Activity {

    private EditText mTxtBoxName;
    private EditText mTxtBoxComment;
    private Button mButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fonc1);

        mTxtBoxName = (EditText) findViewById(R.id.activity_fonc1_txtBox_name);
        mTxtBoxComment = (EditText) findViewById(R.id.activity_fonc1_txtBox_commentaire);
        mButton = (Button) findViewById(R.id.activity_fonc1_button);


        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent output = new Intent();
                output.putExtra("mail", mTxtBoxName.getText().toString());
                output.putExtra("commentaire", mTxtBoxComment.getText().toString());
                setResult(2, output);
                finish();
            }
        });

    }
}
