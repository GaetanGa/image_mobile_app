package fr.garcia.gaetan.image_mobile_app;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.*;

public class MainActivity extends AppCompatActivity {

    //Declaration des elements de l application :
    private Button mCaptureButton;
    private Button mLibraryButton;
    private Button mSearchButton;
    private Button mGetButton;
    private Button mfonc1Button;
    private Button mfonc2Button;
    public TextView txtRelevant1;
    public TextView txtRelevant2;
    public TextView txtRelevant3;
    public TextView txtNonRelevant1;
    public TextView txtNonRelevant2;
    public TextView txtNonRelevant3;
    public ImageView mDisplayImageView;
    public ImageView mDisplayImageView2;
    public ImageView mDisplayImageView3;
    public static final int PICK_IMAGE = 2;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int CTE_CAMERA = 0;
    static final int CTE_GALLERY = 1;
    public PostImage postImage;
    public String mail = null;
    public String comment = null;
    public Boolean feedbackimg1;
    public Boolean feedbackimg2;
    public Boolean feedbackimg3;
    public int relevant1;
    public int relevant2;
    public int relevant3;
    public int non_relevant1;
    public int non_relevant2;
    public int non_relevant3;

    private static final MediaType MEDIA_TYPE_PLAINTEXT = MediaType.parse("application/octet-stream");
    private final OkHttpClient client = new OkHttpClient();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mCaptureButton = (Button) findViewById(R.id.activity_main_capture_button);
        mLibraryButton = (Button) findViewById(R.id.activity_main_library_button);
        mSearchButton = (Button) findViewById(R.id.activity_main_search_button);
        mGetButton = (Button) findViewById(R.id.activity_main_get_button);
        mfonc1Button = (Button) findViewById(R.id.activity_main_fonc1_button);
        mfonc2Button = (Button) findViewById(R.id.activity_main_fonc2_button);
        mDisplayImageView = (ImageView) findViewById(R.id.activity_main_image_clothes);
        mDisplayImageView2 = (ImageView) findViewById(R.id.activity_main_image_clothes2);
        mDisplayImageView3 = (ImageView) findViewById(R.id.activity_main_image_clothes3);
        txtRelevant1 = (TextView) findViewById(R.id.activity_main_txtView_R1);
        txtRelevant2 = (TextView) findViewById(R.id.activity_main_txtView_R2);
        txtRelevant3 = (TextView) findViewById(R.id.activity_main_txtView_R3);
        txtNonRelevant1 = (TextView) findViewById(R.id.activity_main_txtView_NR1);
        txtNonRelevant2 = (TextView) findViewById(R.id.activity_main_txtView_NR2);
        txtNonRelevant3 = (TextView) findViewById(R.id.activity_main_txtView_NR3);

        mCaptureButton.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View view) {

                if (checkSelfPermission(android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, CTE_CAMERA);
                } else {
                    requestPermissions(new String[]{android.Manifest.permission.CAMERA}, REQUEST_IMAGE_CAPTURE);
                }
            }
        });

        mLibraryButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {

                if (checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    mDisplayImageView.setImageBitmap(null); mDisplayImageView2.setImageBitmap(null); mDisplayImageView3.setImageBitmap(null);
                    txtNonRelevant1.setText("");txtNonRelevant2.setText("");txtNonRelevant3.setText("");txtRelevant1.setText("");txtRelevant2.setText("");txtRelevant3.setText("");
                    Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, CTE_GALLERY);

                } else {
                    requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, PICK_IMAGE);
                }

            }
        });

        mSearchButton.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {

                if (mail != null){
                    SendImage sendImage = new SendImage(mDisplayImageView);
                    sendImage.execute();
                }
                else{
                    postImage = new PostImage(mDisplayImageView);
                        postImage.execute();
                }
            }
        });

        mGetButton.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {

                String urlLink = ("http://192.168.0.45:8000/apiImage/get/" + postImage.valueToGet);
                LoadImage loadImagine = new LoadImage(mDisplayImageView);
                loadImagine.execute(urlLink);

                txtRelevant1.setText(Integer.toString(relevant1));
                txtNonRelevant1.setText(Integer.toString(non_relevant1));
                txtRelevant2.setText(Integer.toString(relevant2));
                txtNonRelevant2.setText(Integer.toString(non_relevant2));
                txtRelevant3.setText(Integer.toString(relevant3));
                txtNonRelevant3.setText(Integer.toString(non_relevant3));
            }
        });

        mfonc1Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent activity2Intent = new Intent(getApplicationContext(), fonc1.class);
                startActivityForResult(activity2Intent, 3);
            }
        });

        mfonc2Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent activity2Intent = new Intent(getApplicationContext(), fonc2.class);
                activity2Intent.putExtra("code", postImage.valueToGet);
                startActivityForResult(activity2Intent, 4);
            }
        });
    }

    //Send image to be stored with mail and comment
    private class SendImage extends AsyncTask<String, Void, Void> {
        ImageView imageView;
        Bitmap bitmap;

        public SendImage(ImageView ivResult) {
            this.imageView = ivResult;
            this.bitmap = ((BitmapDrawable) this.imageView.getDrawable()).getBitmap();
        }

        @Override
        protected Void doInBackground(String... strings) {

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            this.bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] imageInByte = baos.toByteArray();
            String base64String = Base64.encodeToString(imageInByte, Base64.DEFAULT);

            JSONObject postData = new JSONObject();

            try{
                postData.put("mail", mail);
                postData.put("commentaire", comment);
                postData.put("image", base64String);
            } catch (JSONException e){
                e.printStackTrace();
            }


            Request request = new Request.Builder()
                    .url("http://192.168.0.45:8000/apiImage/add/")
                    .post(RequestBody.create(postData.toString(), MEDIA_TYPE_PLAINTEXT))
                    .build();

            try {
                client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    //Send image to be analyzed and possible to get the result
    private class PostImage extends AsyncTask<String, Void, Void> {
        ImageView imageView;
        Bitmap bitmap;
        String valueToGet;

        public PostImage(ImageView ivResult) {
            this.imageView = ivResult;
            this.bitmap = ((BitmapDrawable) this.imageView.getDrawable()).getBitmap();
        }

        @Override
        protected Void doInBackground(String... strings) {

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            this.bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] imageInByte = baos.toByteArray();

            Request request = new Request.Builder()
                    .url("http://192.168.0.45:8000/apiImage/post/")
                    .post(RequestBody.create(imageInByte, MEDIA_TYPE_PLAINTEXT))
                    .build();

            try {
                Response response = client.newCall(request).execute();
                String jsonData = response.body().string();
                JSONObject Jobject = new JSONObject(jsonData);
                Object Jarray = Jobject.get("Ressource_name");
                this.valueToGet = Jarray.toString();

                relevant1 = Jobject.getInt("relevant1");
                non_relevant1 = Jobject.getInt("non_relevant1");
                relevant2 = Jobject.getInt("relevant2");
                non_relevant2 = Jobject.getInt("non_relevant2");
                relevant3 = Jobject.getInt("relevant3");
                non_relevant3 = Jobject.getInt("non_relevant3");


            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }


    //Download image with API GET from Django server
    private class LoadImage extends AsyncTask<String, Void, ArrayList<Bitmap>> {
        ImageView imageView;
        public LoadImage(ImageView ivResult) {
            this.imageView = ivResult;
        }

        @Override
        protected ArrayList<Bitmap> doInBackground(String... strings) {

            ArrayList<Bitmap> bitmapArray = new ArrayList<Bitmap>();
            String urlLink = strings[0];
            Bitmap bitmap = null;

            for (int i=0; i<3; i++){
                try {
                    InputStream inputStream = new java.net.URL((urlLink+"/"+i)).openStream();
                    bitmap = BitmapFactory.decodeStream(inputStream);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                bitmapArray.add(bitmap);
            }

            return bitmapArray;
        }

        @Override
        protected void onPostExecute(ArrayList<Bitmap> bitmap) {
            mDisplayImageView.setImageBitmap(bitmap.get(0));
            mDisplayImageView2.setImageBitmap(bitmap.get(1));
            mDisplayImageView3.setImageBitmap(bitmap.get(2));
        }
    }

    //Manage the result of other activities
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //rajouter resultCode
        if (requestCode == 0 && resultCode == Activity.RESULT_OK) {
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            mDisplayImageView.setImageBitmap(bitmap);


        } else if (requestCode == 1 && resultCode == Activity.RESULT_OK) {

            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            Bitmap bitmap = BitmapFactory.decodeFile(picturePath);

            mDisplayImageView.setImageBitmap(bitmap);


        }else if (requestCode == 3){

            mail = data.getStringExtra("mail");
            comment = data.getStringExtra("commentaire");

        }else if (requestCode == 4){

        }
    }

}